import { Component } from '@angular/core';

@Component({
  selector: 'rap-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'rap';
}
