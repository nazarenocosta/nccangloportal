import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, PreloadAllModules } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import {LocationStrategy, HashLocationStrategy} from '@angular/common'

import {ROUTES} from './app.routes'

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { ExposicoesComponent } from './exposicoes/exposicoes.component';

import { ExposicoesService } from './exposicoes/exposicoes.service';
import { ExposicaoComponent } from './exposicoes/exposicao/exposicao.component';
import { RankingsComponent } from './rankings/rankings.component';
import { ResultadoExpositorComponent } from './exposicoes/resultado-expositor/resultado-expositor.component';

import {DataTableModule, SharedModule} from 'primeng/primeng';
import { ResultadoCriadorComponent } from './exposicoes/resultado-criador/resultado-criador.component';
import { ResultadoFemeaComponent } from './exposicoes/resultado-femea/resultado-femea.component';
import { ResultadoMachoComponent } from './exposicoes/resultado-macho/resultado-macho.component';
import { ResultadoProgeneComponent } from './exposicoes/resultado-progene/resultado-progene.component';

import { RankingsService } from './rankings/rankings.service';
import { MelhoresCriadoresComponent } from './rankings/melhores-criadores/melhores-criadores.component';
import { MelhoresExpositoresComponent } from './rankings/melhores-expositores/melhores-expositores.component';
import { MelhoresFemeasComponent } from './rankings/melhores-femeas/melhores-femeas.component';
import { MelhoresMachosComponent } from './rankings/melhores-machos/melhores-machos.component';
import { MelhoresMatrizesComponent } from './rankings/melhores-matrizes/melhores-matrizes.component';
import { MelhoresReprodutoresComponent } from './rankings/melhores-reprodutores/melhores-reprodutores.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ExposicoesComponent,
    ExposicaoComponent,
    RankingsComponent,
    ResultadoExpositorComponent,
    ResultadoCriadorComponent,
    ResultadoFemeaComponent,
    ResultadoMachoComponent,
    ResultadoProgeneComponent,
    MelhoresCriadoresComponent,
    MelhoresExpositoresComponent,
    MelhoresFemeasComponent,
    MelhoresMachosComponent,
    MelhoresMatrizesComponent,
    MelhoresReprodutoresComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,

    DataTableModule,
    SharedModule,

    RouterModule.forRoot(ROUTES, {preloadingStrategy: PreloadAllModules})
  ],
  providers: [ExposicoesService, RankingsService,{provide: LOCALE_ID, useValue: 'pt-BR'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
