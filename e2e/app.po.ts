import { browser, by, element } from 'protractor';

export class PortalPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('rap-root h1')).getText();
  }
}
