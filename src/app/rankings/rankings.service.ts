import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { RANKING_API } from '../app.api';
import { ErrorHandler } from '../app.error-handler';

@Injectable()
export class RankingsService {


    constructor(private http: Http) { }

    melhoresCriadores() {
        return this.http.get(`${RANKING_API}/rankingCriadores`)
            .map(response => {
                const resources = response.json();
                return resources;
            })
            .catch(ErrorHandler.handleError);
    }

    melhoresExpositores() {
        return this.http.get(`${RANKING_API}/rankingExpositores`)
            .map(response => {
                const resources = response.json();
                return resources;
            })
            .catch(ErrorHandler.handleError);
    }

    melhoresMachos() {
        return this.http.get(`${RANKING_API}/rankingAnimaisMachos`)
            .map(response => {
                const resources = response.json();
                return resources;
            })
            .catch(ErrorHandler.handleError);
    }

    melhoresFemeas() {
        return this.http.get(`${RANKING_API}/rankingAnimaisFemeas`)
            .map(response => {
                const resources = response.json();
                return resources;
            })
            .catch(ErrorHandler.handleError);
    }

    melhoresMatrizes() {
        return this.http.get(`${RANKING_API}/rankingAnimaisMatrizes`)
            .map(response => {
                const resources = response.json();
                return resources;
            })
            .catch(ErrorHandler.handleError);
    }

    melhoresReprodutores() {
        return this.http.get(`${RANKING_API}/rankingAnimaisReprodutores`)
            .map(response => {
                const resources = response.json();
                return resources;
            })
            .catch(ErrorHandler.handleError);
    }
}
