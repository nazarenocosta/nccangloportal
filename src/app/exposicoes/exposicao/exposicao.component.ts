import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'

import { ExposicoesService } from './../exposicoes.service';


@Component({
  selector: 'rap-exposicao',
  templateUrl: './exposicao.component.html'
})
export class ExposicaoComponent implements OnInit {

  exposicao: any = {}
  exposicaoId: any = ''

  constructor(private exposicoesService: ExposicoesService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.exposicaoId = this.route.snapshot.params['id']    
    this.exposicoesService.exposicaoById(this.route.snapshot.params['id'])
      .subscribe(exposicao => {
        this.exposicao = exposicao        
      })        
  }  
  
  getDescricao () {
    return this.exposicao.descricao
  }
}
