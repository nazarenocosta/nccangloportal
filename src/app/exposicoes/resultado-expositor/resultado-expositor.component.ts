import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'

import { ExposicoesService } from './../exposicoes.service';
import {DataTableModule,SharedModule, Header} from 'primeng/primeng';

@Component({
  selector: 'rap-resultado-expositor',
  templateUrl: './resultado-expositor.component.html'
})
export class ResultadoExpositorComponent implements OnInit {

  expositores: Array<any>

  constructor(private exposicoesService: ExposicoesService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.exposicoesService.resultadoExpositoresByExposicao(this.route.parent.snapshot.params['id'])
      .subscribe(expositores => {
        this.expositores = expositores.expositores;
        this.expositores.sort(function compare(a, b) {

          const totalPontosA = a.totalPontos;
          const totalPontosB = b.totalPontos;

          let comparison = 0;
          if (totalPontosA > totalPontosB) {
            comparison = 1;
          } else if (totalPontosA < totalPontosB) {
            comparison = -1;
          }
          return comparison * -1;
        });
      });
  }

}
