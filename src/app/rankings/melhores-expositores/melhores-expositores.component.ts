import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { RankingsService } from '../rankings.service';
import { DataTableModule, Header } from 'primeng/primeng';

@Component({
  selector: 'rap-melhores-expositores',
  templateUrl: './melhores-expositores.component.html'
})
export class MelhoresExpositoresComponent implements OnInit {
  expositores: Array<any>;

  constructor(
    private rankingsService: RankingsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.rankingsService.melhoresExpositores().subscribe(expositores => {
      this.expositores = expositores;
      this.expositores.sort(function compare(a, b) {

        const totalPontosA = a.pontos;
        const totalPontosB = b.pontos;

        let comparison = 0;
        if (totalPontosA > totalPontosB) {
          comparison = 1;
        } else if (totalPontosA < totalPontosB) {
          comparison = -1;
        }
        return comparison * -1;
      });
    });
  }
}
