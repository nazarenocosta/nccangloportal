import { Injectable } from '@angular/core';
import {Http} from '@angular/http'

import {Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import {RANKING_API} from '../app.api'
import {ErrorHandler} from '../app.error-handler'

@Injectable()
export class ExposicoesService {

  private exposicaoSource = new BehaviorSubject<any>({})
  currentExposicao = this.exposicaoSource.asObservable()
  
  constructor(private http: Http) { }

  changeCurrentExposicao(exposicao: any) {
    this.exposicaoSource.next(exposicao)
  }

  exposicoes(): Observable<any> {
    return this.http.get(`${RANKING_API}/exposicoes?select=descricao%20cidade%20dataInicial%20dataFinal%20cartaz&publicada=S`)
      .map(response => response.json())
      .catch(ErrorHandler.handleError)
  }

  exposicaoById(id: string) {
    return this.http.get(`${RANKING_API}/exposicoes?_id=${id}`)
    .map(response => {
      var resources = response.json()
      return (resources.length === 0 ? null : resources[0]);
    })
    .catch(ErrorHandler.handleError)
  }

  resultadoExpositoresByExposicao(id: string) {
    return this.http.get(`${RANKING_API}/exposicoes?_id=${id}&select=descricao%20expositores.nome%20expositores.totalPontos`)
      .map(response => {
        var resources = response.json()
        return (resources.length === 0 ? null : resources[0]);
      })
      .catch(ErrorHandler.handleError)
  }

  resultadoCriadoresByExposicao(id: string) {
    return this.http.get(`${RANKING_API}/exposicoes?_id=${id}&select=descricao%20criadores.nome%20criadores.totalPontos`)
      .map(response => {
        var resources = response.json()
        return (resources.length === 0 ? null : resources[0]);
      })
      .catch(ErrorHandler.handleError)
  }

  resultadoAnimaisByExposicao(id: string) {    
    return this.http.get(`${RANKING_API}/exposicoes?_id=${id}&select=expositores.animais.registro%20expositores.animais.nome%20expositores.animais.sexo%20expositores.animais.categoria.nome%20expositores.animais.premiacao.nome%20expositores.animais.premiacao.pontos%20expositores.animais.idade.meses`)
    .map(response => {
      var resources = response.json()
      return (resources.length === 0 ? null : resources[0].expositores);
    })
    .catch(ErrorHandler.handleError)
  }

  resultadoProgenesByExposicao(id: string) {    
    return this.http.get(`${RANKING_API}/exposicoes?_id=${id}&select=expositores.progenes.registro%20expositores.progenes.nome%20expositores.progenes.tipo%20expositores.progenes.premiacao.nome%20expositores.progenes.premiacao.pontos%20expositores.progenes.animais`)
    .map(response => {
      var resources = response.json()
      return (resources.length === 0 ? null : resources[0].expositores);
    })
    .catch(ErrorHandler.handleError)
  }

}
