import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import { RankingsService } from '../rankings.service';

@Component({
  selector: 'rap-melhores-machos',
  templateUrl: './melhores-machos.component.html'
})
export class MelhoresMachosComponent implements OnInit {

  animais: Array<any>;

    constructor(private rankingsService: RankingsService,
      private route: ActivatedRoute) { }

    ngOnInit() {
      this.rankingsService.melhoresMachos()
        .subscribe(animais => {
          const filtrados = animais.filter(animal => animal.pontos > 0);
          this.animais = filtrados;
          this.animais.sort(function compare(a, b) {

            const totalPontosA = a.pontos;
            const totalPontosB = b.pontos;

            let comparison = 0;
            if (totalPontosA > totalPontosB) {
              comparison = 1;
            } else if (totalPontosA < totalPontosB) {
              comparison = -1;
            }
            return comparison * -1;
          });
        });
    }

}
