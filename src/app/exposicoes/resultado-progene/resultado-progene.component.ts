import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ExposicoesService } from './../exposicoes.service';
import { DataTableModule, SharedModule, Header } from 'primeng/primeng';

@Component({
  selector: 'rap-resultado-progene',
  templateUrl: './resultado-progene.component.html'
})
export class ResultadoProgeneComponent implements OnInit {

  animais: Array<any>;

  constructor(private exposicoesService: ExposicoesService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.exposicoesService.resultadoProgenesByExposicao(this.route.parent.snapshot.params['id'])
      .subscribe(expositores => {
        this.animais = expositores.map(function (expositor) {
          return expositor.progenes;
        }).reduce(function (pre, a) {
          return pre.concat(a);
        });
        console.log(this.animais);
      });
  }

}
