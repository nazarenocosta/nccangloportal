import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { RankingsService } from './../rankings.service';
import { DataTableModule, SharedModule, Header } from 'primeng/primeng';

@Component({
  selector: 'rap-melhores-reprodutores',
  templateUrl: './melhores-reprodutores.component.html'
})
export class MelhoresReprodutoresComponent implements OnInit {

  animais: Array<any>;

    constructor(private rankingsService: RankingsService,
      private route: ActivatedRoute) { }

    ngOnInit() {
      this.rankingsService.melhoresReprodutores()
        .subscribe(animais => {
          const filtrados = animais.filter(animal => animal.pontos > 0);
          this.animais = filtrados;
          this.animais.sort(function compare(a, b) {

            const totalPontosA = a.pontos;
            const totalPontosB = b.pontos;

            let comparison = 0;
            if (totalPontosA > totalPontosB) {
              comparison = 1;
            } else if (totalPontosA < totalPontosB) {
              comparison = -1;
            }
            return comparison * -1;
          });
        });
    }

}
