import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ExposicoesService } from './../exposicoes.service';
import { DataTableModule, SharedModule, Header } from 'primeng/primeng';

@Component({
  selector: 'rap-resultado-femea',
  templateUrl: './resultado-femea.component.html'
})
export class ResultadoFemeaComponent implements OnInit {

  animais: Array<any>;

  constructor(private exposicoesService: ExposicoesService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.exposicoesService.resultadoAnimaisByExposicao(this.route.parent.snapshot.params['id'])
      .subscribe(expositores => {
        this.animais = expositores.map(function (expositor) {
          return expositor.animais;
        }).reduce(function (pre, a) {
          return pre.concat(a);
        }).filter(animais => animais.sexo === 'FÊMEA' && animais.premiacao.length > 0);

        this.animais.sort(function compare(a, b) {

          const totalPontosA = a.premiacao[0].pontos;
          const totalPontosB = b.premiacao[0].pontos;

          let comparison = 0;
          if (totalPontosA > totalPontosB) {
            comparison = 1;
          } else if (totalPontosA < totalPontosB) {
            comparison = -1;
          }
          return comparison * -1;
        });

        console.log(this.animais);
      });
  }

  public getPremiacao(animal): String {
    const premios = animal.premiacao.map(premio => premio.nome);
    let result = '';
    for (let i = 0; i < premios.length; i++) {
      result = result + premios[i] + ' <br>';
    }
    return result;
  }

}
