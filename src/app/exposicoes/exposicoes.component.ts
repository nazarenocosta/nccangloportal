import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';

import { ExposicoesService } from './exposicoes.service';

@Component({
  selector: 'rap-exposicoes',
  templateUrl: './exposicoes.component.html'
})
export class ExposicoesComponent implements OnInit {

  exposicoes: any[]
  exposicao: any

  constructor(private exposicoesService: ExposicoesService) { }

  ngOnInit() {
    this.exposicoesService.exposicoes()
      .subscribe(exposicoes => this.exposicoes = exposicoes)

    this.exposicoesService.currentExposicao
      .subscribe(exposicao => this.exposicao = exposicao)      
  }

  selectExposicao(exposicao: any) {
    this.exposicoesService.changeCurrentExposicao(exposicao)
  }
}
