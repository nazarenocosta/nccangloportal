import { combineAll } from 'rxjs/operator/combineAll';
import {Routes} from '@angular/router'

import {HomeComponent} from './home/home.component'
import { ExposicoesComponent } from './exposicoes/exposicoes.component';
import { ExposicaoComponent } from './exposicoes/exposicao/exposicao.component';
import { RankingsComponent } from './rankings/rankings.component';
import { ResultadoExpositorComponent } from './exposicoes/resultado-expositor/resultado-expositor.component';
import { ResultadoCriadorComponent } from './exposicoes/resultado-criador/resultado-criador.component';
import { ResultadoFemeaComponent } from './exposicoes/resultado-femea/resultado-femea.component';
import { ResultadoMachoComponent } from './exposicoes/resultado-macho/resultado-macho.component';
import { ResultadoProgeneComponent } from './exposicoes/resultado-progene/resultado-progene.component';
import { MelhoresCriadoresComponent } from './rankings/melhores-criadores/melhores-criadores.component';
import { MelhoresExpositoresComponent } from './rankings/melhores-expositores/melhores-expositores.component';
import { MelhoresFemeasComponent } from './rankings/melhores-femeas/melhores-femeas.component';
import { MelhoresMachosComponent } from './rankings/melhores-machos/melhores-machos.component';
import { MelhoresMatrizesComponent } from './rankings/melhores-matrizes/melhores-matrizes.component';
import { MelhoresReprodutoresComponent } from './rankings/melhores-reprodutores/melhores-reprodutores.component';

export const ROUTES: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'exposicoes', component: ExposicoesComponent},
  {path: 'exposicoes/:id', component: ExposicaoComponent,
    children: [      
      {path: 'resultadoExpositores', component: ResultadoExpositorComponent},
      {path: 'resultadoCriadores', component: ResultadoCriadorComponent},
      {path: 'resultadoFemeas', component: ResultadoFemeaComponent},
      {path: 'resultadoMachos', component: ResultadoMachoComponent},
      {path: 'resultadoProgenes', component: ResultadoProgeneComponent}
    ]},
  {path: 'rankings', component: RankingsComponent,
    children: [
      {path: 'melhoresCriadores', component: MelhoresCriadoresComponent},
      {path: 'melhoresExpositores', component: MelhoresExpositoresComponent},
      {path: 'melhoresReprodutores', component: MelhoresReprodutoresComponent},
      {path: 'melhoresMatrizes', component: MelhoresMatrizesComponent},
      {path: 'melhoresFemeas', component: MelhoresFemeasComponent},
      {path: 'melhoresMachos', component: MelhoresMachosComponent}
    ]},
]