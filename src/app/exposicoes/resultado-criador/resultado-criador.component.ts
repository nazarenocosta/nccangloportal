import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import { ExposicoesService } from './../exposicoes.service';
import {DataTableModule, Header} from 'primeng/primeng';

@Component({
  selector: 'rap-resultado-criador',
  templateUrl: './resultado-criador.component.html'
})

export class ResultadoCriadorComponent implements OnInit {

  criadores: Array<any>;

    constructor(private exposicoesService: ExposicoesService,
      private route: ActivatedRoute) { }

    ngOnInit() {
      this.exposicoesService.resultadoCriadoresByExposicao(this.route.parent.snapshot.params['id'])
        .subscribe(criadores => {
          this.criadores = criadores.criadores;
          this.criadores.sort(function compare(a, b) {

            const totalPontosA = a.totalPontos;
            const totalPontosB = b.totalPontos;

            let comparison = 0;
            if (totalPontosA > totalPontosB) {
              comparison = 1;
            } else if (totalPontosA < totalPontosB) {
              comparison = -1;
            }
            return comparison * -1;
          });
        });
    }

}
