import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { RankingsService } from '../rankings.service';
import { DataTableModule, Header } from 'primeng/primeng';

@Component({
  selector: 'rap-melhores-criadores',
  templateUrl: './melhores-criadores.component.html'
})
export class MelhoresCriadoresComponent implements OnInit {
  criadores: Array<any>;
  constructor(
    private rankingsService: RankingsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.rankingsService.melhoresCriadores().subscribe(criadores => {
      this.criadores = criadores;
      this.criadores.sort(function compare(a, b) {

        const totalPontosA = a.pontos;
        const totalPontosB = b.pontos;

        let comparison = 0;
        if (totalPontosA > totalPontosB) {
          comparison = 1;
        } else if (totalPontosA < totalPontosB) {
          comparison = -1;
        }
        return comparison * -1;
      });
    });
  }
}
