const express = require('express');
const app = express();
const path = require('path');
const fs = require('fs');

const port = process.env.PORT || 4200;

app.use(express.static(__dirname + '/dist'));

app.use('/', function (req, res, next) {
    res.sendFile('/dist/index.html', { root: __dirname });
});
/*
app.use('/portal', function (req, res, next) {
    res.sendFile('/dist/index.html', { root: __dirname });
});

app.use('/login', function (req, res, next) {
    res.sendFile('/dist/index.html', { root: __dirname });
});

app.use('/env-vars', function (req, res, next) {
    res.status(200).json({
        apiURL: process.env.APIURL,
        webURL: process.env.WEBURL,
        stripeKey: process.env.STRIPEKEY
    });
});
*/

app.listen(port, function () {
    console.log('Portal is running on port', port);
});