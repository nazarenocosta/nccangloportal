import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { RankingsService } from '../rankings.service';
import { DataTableModule, SharedModule, Header } from 'primeng/primeng';

@Component({
  selector: 'rap-melhores-femeas',
  templateUrl: './melhores-femeas.component.html'
})
export class MelhoresFemeasComponent implements OnInit {

  animais: Array<any>;

  constructor(private rankingsService: RankingsService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.rankingsService.melhoresFemeas()
      .subscribe(animais => {
        const filtrados = animais.filter(animal => animal.pontos > 0);
        this.animais = filtrados;

        this.animais.sort(function compare(a, b) {

          const totalPontosA = a.pontos;
          const totalPontosB = b.pontos;

          let comparison = 0;
          if (totalPontosA > totalPontosB) {
            comparison = 1;
          } else if (totalPontosA < totalPontosB) {
            comparison = -1;
          }
          return comparison * -1;
        });
      });
  }

}
